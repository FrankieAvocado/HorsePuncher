﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class punch_stats_script : MonoBehaviour {

	private Text punchStats;
	// Use this for initialization
	void Start () {
		punchStats = GetComponent<Text> ();
		punchStats.text = "Horses Punched: " + GlobalStats.HorsesPunched;
	}
	
	// Update is called once per frame
	void Update () {
		punchStats.text = "Horses Punched: " + GlobalStats.HorsesPunched;
	}
}
