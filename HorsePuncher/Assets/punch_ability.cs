﻿using UnityEngine;
using System.Collections;
using AnimatedPixelPack;

public class punch_ability : MonoBehaviour {

	private Character puncher;
	public Transform SuccessText;
	private Renderer successTextRenderer;
	private TextMesh instructionText;
	private string currentPuncheeName = "";
	private bool alreadyShowingInstructions = false;


	// Use this for initialization
	void Start () {
		successTextRenderer = SuccessText.GetComponent<Renderer> ();
		successTextRenderer.enabled = false;
		puncher = GetComponent<Character> ();
		instructionText = puncher.transform.Find ("InstructionText").GetComponent<TextMesh>();
	}

	public void SetPuncheeName(string name){
		currentPuncheeName = name;
		if (string.IsNullOrEmpty (name) && alreadyShowingInstructions) {
			instructionText.gameObject.GetComponent<Renderer>().enabled = false;
			alreadyShowingInstructions = false;
		}
	}

	void ShowInstruction(){
		if (!string.IsNullOrEmpty(currentPuncheeName)) {
			if(!GlobalStats.HorsesDefeated.Contains (currentPuncheeName)){
				instructionText.text = "Press 'p' to punch " + currentPuncheeName + ".";
			}
			else{
				instructionText.text = "You've already punched " + currentPuncheeName + "!";
			}
			instructionText.gameObject.GetComponent<Renderer>().enabled = true;
			alreadyShowingInstructions = true;
		}
	}
	
	// Update is called once per frame
	void Update () {


		if (puncher != null && !string.IsNullOrEmpty (currentPuncheeName)) {
			if(!alreadyShowingInstructions)
			{
				ShowInstruction();
			}

			if (Input.GetKeyDown (KeyCode.P) && puncher.IsAllowedToAttack && !GlobalStats.HorsesDefeated.Contains(currentPuncheeName)) {
				puncher.PunchHorse ();
				GlobalStats.HorsesDefeated.Add (currentPuncheeName);
				puncher.IsAllowedToAttack = false;
				puncher.IsAllowedToMove = false;
				puncher.PlaySound (Character.SoundClip.Punch);

				if (instructionText != null) {
					instructionText.gameObject.GetComponent<Renderer> ().enabled = false;
				}

				//if (successTextRenderer != null) {
				//	successTextRenderer.enabled = true;
				//}

				GlobalStats.HorsesPunched += 1;

				Invoke ("ReturnToMainMenu", 2.0f);

			}
		} 
	}

	void ReturnToMainMenu(){
		Application.LoadLevel (2);
	}
}
