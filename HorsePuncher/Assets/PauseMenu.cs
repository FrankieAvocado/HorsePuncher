﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour {

    private AudioSource bgMusic;
    public Canvas pauseMenu;

	// Use this for initialization
	void Start () {
        bgMusic = GetComponent<AudioSource>();
        pauseMenu.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Pause"))
        {
            GamePauseState.isPaused = !GamePauseState.isPaused;
            if (GamePauseState.isPaused)
            {
                bgMusic.Pause();
                pauseMenu.enabled = true;
            }
            else
            {
                bgMusic.UnPause();
                pauseMenu.enabled = false;
            }
        }

        if (GamePauseState.isPaused)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
	}

    public void Quit()
    {
        Application.Quit();
    }

    public void Restart()
    {
        Application.LoadLevel(1);
        GamePauseState.isPaused = false;
    }

    public void MainMenu()
    {
        Application.LoadLevel(2);
        GamePauseState.isPaused = false;
    }
}

public static class GamePauseState
{
    public static bool isPaused = false;
}
