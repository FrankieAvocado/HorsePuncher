﻿using UnityEngine;
using System.Collections;
using AnimatedPixelPack;

public class FollowTarget : MonoBehaviour {

	public Transform FollowThis;
	public float dampTime = 0.15f;
	private Vector3 velocity = Vector3.zero;
	public float verticalOffset = 2f;
	public float minimumY = -20f;
	private AnimatedPixelPack.Character targetCharacter;

	// Use this for initialization
	void Start () {
		if(FollowThis != null){
			targetCharacter = FollowThis.parent.GetComponent<AnimatedPixelPack.Character>();

		}
	}
	
	// Update is called once per frame
	void Update () {
		if (FollowThis != null) {
			Vector3 point = Camera.main.ViewportToWorldPoint(FollowThis.position + new Vector3(0f, verticalOffset, 0f));
			Vector3 delta = FollowThis.position - Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 99));
			Vector3 destination = transform.position + delta;
			transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);

			if(destination.y < minimumY && targetCharacter != null){
				targetCharacter.ApplyDamage(30, 1);
				targetCharacter.ResetToLastJumpPosition();
			}
		}
	}

	void FixedUpdate(){

	}
}
