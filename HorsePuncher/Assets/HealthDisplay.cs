﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AnimatedPixelPack;
using UnityEngine.UI;

public class HealthDisplay : MonoBehaviour {

	public AnimatedPixelPack.Character ThisCharacter;
	public int HealthPerHeart = 20;
	private UnityEngine.UI.Text healthText;
	public Transform SingleHeartPrefab;
	private List<SingleHeartCalculator> allHearts = new List<SingleHeartCalculator>();

	// Use this for initialization
	void Start () {
		healthText = this.GetComponent<UnityEngine.UI.Text> ();
		if (ThisCharacter != null) {
			int heartCount = Mathf.RoundToInt(ThisCharacter.MaxHealth / HealthPerHeart);
			for(int i = 0; i < heartCount; i++){
				Transform newHeart = ((GameObject)Instantiate(SingleHeartPrefab.gameObject)).transform;
				newHeart.parent = this.transform;
				newHeart.localPosition = new Vector3(20 * i, 0);
				SingleHeartCalculator heartScript = newHeart.GetComponent<SingleHeartCalculator>();
				allHearts.Add(heartScript);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (ThisCharacter != null) {
			convertNumberToHearts(ThisCharacter.CurrentHealth);
		}
	}

	private void convertNumberToHearts(int number){

		for (int i = 0; i < allHearts.Count; i++) {
			int thisHeartValue = (i + 1) * HealthPerHeart;

			int leftOver = number % HealthPerHeart;

			if(thisHeartValue <= number){
				allHearts[i].SetHeartCondition(SingleHeartCalculator.HeartCondition.full);
			}
			else if(thisHeartValue > number && (thisHeartValue - number % HealthPerHeart) == number){
				allHearts[i].SetHeartCondition(SingleHeartCalculator.HeartCondition.half);
			}
			else{
				allHearts[i].SetHeartCondition(SingleHeartCalculator.HeartCondition.empty);
			}
		}
	}
}
