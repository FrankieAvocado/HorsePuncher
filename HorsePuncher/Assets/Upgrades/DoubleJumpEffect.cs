﻿using UnityEngine;
using System.Collections;

public class DoubleJumpEffect : MonoBehaviour {

	public class doubleJump : IUpgradeEffect{
		public void ApplyEffect(AnimatedPixelPack.Character effectTarget){
			effectTarget.EnabledDoubleJump = true;
		}
	}
	
	public void Start(){
		doubleJump primaryEffect = new doubleJump ();
		transform.GetComponent<AwesomeUpgrade> ().effects.Add (primaryEffect);
	}
}
