﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class AwesomeUpgrade : MonoBehaviour {

	public string HorseKeeperName = "";
	public string Name = "Upgrade";
	public List<IUpgradeEffect> effects = new List<IUpgradeEffect>();
	private AudioSource SuccessSoundClip;
	private AudioSource FailSoundClip;

	public void ApplyUpgrade(AnimatedPixelPack.Character upgradeTarget){

			foreach(IUpgradeEffect effect in effects)
			{
				effect.ApplyEffect(upgradeTarget);
			}
			

	}

	public bool IsPurchased{get; set;}

	public void Update(){

	}

	public void applyText(){

		var buyButton = transform.Find ("Buy Button").GetComponent<UnityEngine.UI.Text> ();

		if (IsPurchased) {
			buyButton.text = "Unlocked!";
		} else if (checkIsUnlockable ()) {
			buyButton.text = "Click to Unlock";
		} else {
			buyButton.text = "Locked";
		}
	}

	public bool checkIsUnlockable(){
		return GlobalStats.HorsesDefeated.Contains (HorseKeeperName) && !IsPurchased;
	}

	public void Start(){

		foreach (var upgrade in GlobalStats.Upgrades) {
			if(upgrade.Name == Name){
				IsPurchased = true;
				break;
			}
		}

		applyText ();

		transform.Find ("Effect Text").GetComponent<UnityEngine.UI.Text> ().text = Name;
		transform.Find ("Cost Text").GetComponent<UnityEngine.UI.Text>().text = "Punch " + HorseKeeperName + " to unlock";
		var soundSubset = transform.Find ("Sounds");

		if (soundSubset != null) {
			Transform successSound = soundSubset.Find ("Success");
			if(successSound != null){
				SuccessSoundClip = successSound.GetComponent<AudioSource>();
			}
			
			Transform failSound = soundSubset.Find ("Failure");
			if(failSound != null){
				FailSoundClip = failSound.GetComponent<AudioSource>();
			}
		}

	}

	public void BuyEffect(){
		if (checkIsUnlockable()) {
			IsPurchased = true;
			GlobalStats.Upgrades.Add (this);

			if (SuccessSoundClip != null) {
				SuccessSoundClip.Play ();
			}
		} else {
			if(FailSoundClip != null){
				FailSoundClip.Play();
			}
		}

		applyText ();
	}

}
