﻿using UnityEngine;
using System.Collections;

public class RunIncreaseEffect : MonoBehaviour {

	public class runIncrease : IUpgradeEffect{
		public void ApplyEffect(AnimatedPixelPack.Character effectTarget){
			effectTarget.WalkSpeed *= 2;
			effectTarget.RunSpeed *= 2;
			effectTarget.RunningJumpPower += 100;
		}
	}

	public void Start(){
		runIncrease primaryEffect = new runIncrease ();
		transform.GetComponent<AwesomeUpgrade> ().effects.Add (primaryEffect);
	}

}
