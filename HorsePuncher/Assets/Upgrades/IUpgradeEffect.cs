﻿using UnityEngine;
using System.Collections;
using AnimatedPixelPack;

public interface IUpgradeEffect {

	void ApplyEffect(AnimatedPixelPack.Character effectTarget);

}
