﻿using UnityEngine;
using System.Collections;

public class JumpIncreaseEffect : MonoBehaviour {

	public class jumpIncrease : IUpgradeEffect{
		public void ApplyEffect(AnimatedPixelPack.Character effectTarget){
			effectTarget.JumpPower += 300;
			effectTarget.RunningJumpPower += 300;
		}
	}

	public void Start(){
		jumpIncrease primaryEffect = new jumpIncrease ();
		transform.GetComponent<AwesomeUpgrade> ().effects.Add (primaryEffect);
	}

}
