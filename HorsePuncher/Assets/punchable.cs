﻿using UnityEngine;
using System.Collections;
using AnimatedPixelPack;

public class punchable : MonoBehaviour {

	public string HorseName = "";
	public TextMesh RelatedInsult;

	// Use this for initialization
	void Start () {
		if (GlobalStats.HorsesDefeated.Contains (HorseName)) {
			if(RelatedInsult != null){
				RelatedInsult.text = "We're cool bro!";
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D other){
		Character collidedCharacter = other.transform.GetComponent<Character> ();
		punch_ability puncher = other.transform.GetComponent<punch_ability> ();
		
		if (collidedCharacter != null) {
			puncher.SetPuncheeName(HorseName);
			collidedCharacter.IsAllowedToAttack = true;
		}
		
	}
	
	void OnTriggerExit2D(Collider2D other){

		Character collidedCharacter = other.transform.GetComponent<Character> ();
		punch_ability puncher = other.transform.GetComponent<punch_ability> ();

		if (collidedCharacter != null) {
			puncher.SetPuncheeName("");
			collidedCharacter.IsAllowedToAttack = false;
		}

	}
}
