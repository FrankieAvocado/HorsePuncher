﻿using UnityEngine;
using System.Collections;

public class parallaxMe : MonoBehaviour {

	public Camera targetCamera;
	public float parallaxAmount = .5f;

	private Vector3 lastCameraPosition;

	// Use this for initialization
	void Start () {
		lastCameraPosition = getCameraPosition();
	}

	private Vector3 getCameraPosition(){
		Vector3 returnMe = Vector3.zero;
		if (targetCamera != null) {
			returnMe = targetCamera.transform.position;
		}
		return returnMe;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		updateParallax ();
	}

	private void updateParallax(){
		Vector3 currentCameraPosition = getCameraPosition ();
		Vector3 distanceMoved = currentCameraPosition - lastCameraPosition;
		distanceMoved.z = 0.0f;  // don't parallax in the z direction

		distanceMoved.x *= parallaxAmount;
		distanceMoved.y *= parallaxAmount;

		transform.position += distanceMoved;
		lastCameraPosition = currentCameraPosition;
	}
}
