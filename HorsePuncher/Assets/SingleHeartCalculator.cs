﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SingleHeartCalculator : MonoBehaviour {

	private UnityEngine.UI.Image empty;
	private UnityEngine.UI.Image half;
	private UnityEngine.UI.Image full;

	// Use this for initialization
	void Start () {
		empty = this.transform.Find("Empty").GetComponent<UnityEngine.UI.Image>();
		half = this.transform.Find("Half").GetComponent<UnityEngine.UI.Image>();
		full = this.transform.Find("Full").GetComponent<UnityEngine.UI.Image>();

		empty.enabled = false;
		half.enabled = false;
		full.enabled = false;
	}

	public enum HeartCondition {empty, half, full};

	public void SetHeartCondition(HeartCondition condition){
		switch (condition) {
			case HeartCondition.empty:
				empty.enabled = true;
				half.enabled = false;
				full.enabled = false;
				break;
			case HeartCondition.half:
				empty.enabled = false;
				half.enabled = true;
				full.enabled = false;
				break;
			case HeartCondition.full:
				empty.enabled = false;
				half.enabled = false;
				full.enabled = true;
				break;
		}
	}

	// Update is called once per frame
	void Update () {
		
	}
}
