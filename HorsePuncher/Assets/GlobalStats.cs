﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalStats : MonoBehaviour {

	public static int HorsesPunched = 0;
	public static List<string> HorsesDefeated = new List<string> ();
	public static List<AwesomeUpgrade> Upgrades = new List<AwesomeUpgrade>();

	void Awake(){
		DontDestroyOnLoad (transform.gameObject);
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
