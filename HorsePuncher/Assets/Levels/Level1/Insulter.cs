﻿using UnityEngine;
using System.Collections;

public class Insulter : MonoBehaviour {

	private TextMesh insultPhrase;

	// Use this for initialization
	void Start () {
		insultPhrase = this.transform.Find ("Phrase").GetComponent<TextMesh> ();
		if (insultPhrase) {
			insultPhrase.gameObject.GetComponent<Renderer> ().enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other){
		punch_ability puncher = other.transform.GetComponent<punch_ability> ();
		
		if (puncher != null) {
			insultPhrase.gameObject.GetComponent<Renderer> ().enabled = true;
		}
		
	}
	
	void OnTriggerExit2D(Collider2D other){

		punch_ability puncher = other.transform.GetComponent<punch_ability> ();
		
		if (puncher != null) {
			insultPhrase.gameObject.GetComponent<Renderer> ().enabled = false;
		}
		
	}
}
