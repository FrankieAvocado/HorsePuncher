﻿using UnityEngine;
using System.Collections;

public class instructions_toggle : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other){
		Transform instructionText = other.transform.Find ("InstructionText");

		if (instructionText != null) {
			instructionText.gameObject.GetComponent<Renderer> ().enabled = true;
		}

	}

	void OnTriggerExit2D(Collider2D other){
		Transform instructionText = other.transform.Find ("InstructionText");
		
		if (instructionText != null) {
			instructionText.gameObject.GetComponent<Renderer> ().enabled = false;
		}
	}
}
